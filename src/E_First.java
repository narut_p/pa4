
public class E_First extends State{
	private WordCounter wordcounter;
	public E_First(WordCounter wordcounter) {
		this.wordcounter = wordcounter;
	}
	public void handle(char c) {
		if (wordcounter.isVowel(c)) {
			wordcounter.setState(wordcounter.vowels);
			wordcounter.syllable++;
		} else if (c == '-') {
			wordcounter.setState(wordcounter.consonant);
		} else if (Character.isLetter(c)) {
			wordcounter.setState(wordcounter.consonant);
			wordcounter.syllable++;
		}
	}
}
