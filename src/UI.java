import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;

public class UI extends JFrame {
	private JLabel nameLabel;
	private JTextField urlText;
	private JButton browseButton, countButton, clearButton;
	private JTextArea textArea;
	private JPanel contentPane;

	public UI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 600, 320);
		contentPane = new JPanel();
		contentPane.setLayout(null);
		initialize();
		this.setVisible(true);
	}

	public void initialize() {
		nameLabel = new JLabel();
		urlText = new JTextField(10);
		browseButton = new JButton("Browse");
		countButton = new JButton("Count");
		clearButton = new JButton("Clear");
		textArea = new JTextArea();

		nameLabel.setBounds(20, 13, 110, 30);
		urlText.setBounds(130, 20, 150, 20);
		browseButton.setBounds(290, 20, 80, 20);
		countButton.setBounds(380, 20, 80, 20);
		clearButton.setBounds(470, 20, 80, 20);
		textArea.setBounds(5, 50, 580, 290);
		nameLabel.setText("File or URL name");

		browseButton.addActionListener(new ActionListener() {
			
			
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser chooseFile = new JFileChooser();
				FileFilter filter = new TextFileFilter();
				chooseFile.setFileFilter(filter);
				chooseFile.showOpenDialog(chooseFile);
				urlText.setText(chooseFile.getSelectedFile().getAbsolutePath());
				 
			}
			public String getFilename() {
				JFileChooser chooseFile = new JFileChooser();
				FileFilter filter = new TextFileFilter();
				chooseFile.addChoosableFileFilter(filter);
				chooseFile.setCurrentDirectory(new File("C:/"));
				
				int result = chooseFile.showDialog(null, "Open");
				if(result == JFileChooser.APPROVE_OPTION)
					return chooseFile.getSelectedFile().getAbsolutePath();
				else return null;
			}

		});

		countButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
				WordCounter wordCounter = new WordCounter();
				
				int syllables = wordCounter.getSyllableCount();

				/** Print a number of syllables and words */
				
				String result = null;
				
				if (urlText.getText().startsWith("http") || urlText.getText().startsWith("ftp")) {
					InputStream fis = null;
					try {
						fis = new URL(urlText.getText()).openStream();
					} catch (MalformedURLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					int wordcount = wordCounter.countWords(fis);
					double readability = (double) 206.835
							- 84.6
							* ((double) wordCounter.getSyllableCount() / (double) wordcount)
							- 1.015
							* ((double) wordcount / (double) wordCounter
									.getSentenceCount());
					result = String.format(
							"%4s %4s\n%4s %d\n%4s %d\n%4s %d\n%4s %4.2f",
							"Filename:", urlText.getText(), "Number of syllables",
							wordCounter.getSyllableCount(), "Number of words",
							wordcount, "Number of Sentences",
							wordCounter.getSentenceCount(), "Flesch index",
							readability);
				} else {
					FileInputStream fis = null;
					try {
						fis = new FileInputStream(new File(urlText.getText()));
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
					int wordcount = wordCounter.countWords(fis);
					double readability = (double) 206.835
							- 84.6
							* ((double) wordCounter.getSyllableCount() / (double) wordcount)
							- 1.015
							* ((double) wordcount / (double) wordCounter
									.getSentenceCount());
					result = String.format(
							"%4s %4s\n%4s %d\n%4s %d\n%4s %d\n%4s %4.2f",
							"Filename:", urlText.getText(), "Number of syllables",
							wordCounter.getSyllableCount(), "Number of words",
							wordcount, "Number of Sentences",
							wordCounter.getSentenceCount(), "Flesch index",
							readability);

				}
				
				textArea.setText(result);
				
				
			}
		});

		clearButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				textArea.setText("");
				urlText.setText("");
			}
		});

		contentPane.add(nameLabel);
		contentPane.add(urlText);
		contentPane.add(browseButton);
		contentPane.add(countButton);
		contentPane.add(clearButton);
		contentPane.add(textArea);
		super.add(contentPane);

	}
	class TextFileFilter extends FileFilter {

		@Override
		public boolean accept(File f) {
			if (f.isDirectory())
				return true;
			if (!f.isFile())
				return false;
			String ext = getExtension(f);
			if (ext.equals("txt"))
				return true;
			if ( ext.equals("htm") ) return true;
			if ( ext.equals("html") ) return true;
			return false;
		}

		@Override
		public String getDescription() {
			return "Text(.txt)Files";
		}
		String getExtension(File f) {
			String filename = f.getName();
			int k = filename.lastIndexOf('.');
			if ( k <= 0 || k >= filename.length() -1 ) return "";
			else return filename.substring(k+1).toLowerCase();
		}


	}
	
}
