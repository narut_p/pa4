import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class Readability {
	public static final WordCounter wordCounter = new WordCounter();

	public static void main(String[] args) {
		if (args.length == 0) {
			startGUI();
		} else {
			startCMD(args);
		}
	}

	private static void startCMD(String[] args) {
		for (String arg : args) {
			if (arg.startsWith("http") || arg.startsWith("ftp")) {
				InputStream fis = null;
				try {
					fis = new URL(arg).openStream();
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				int wordcount = wordCounter.countWords(fis);
				double readability = (double) 206.835
						- 84.6
						* ((double) wordCounter.getSyllableCount() / (double) wordcount)
						- 1.015
						* ((double) wordcount / (double) wordCounter
								.getSentenceCount());
				String result = String.format(
						"%4s %4s\n%4s %d\n%4s %d\n%4s %d\n%4s %4.2f",
						"Filename:", arg, "Number of syllables",
						wordCounter.getSyllableCount(), "Number of words",
						wordcount, "Number of Sentences",
						wordCounter.getSentenceCount(), "Flesch index",
						readability);

				System.out.println(result);
			} else {
				FileInputStream fis = null;
				try {
					fis = new FileInputStream(new File(arg));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				int wordcount = wordCounter.countWords(fis);
				double readability = (double) 206.835
						- 84.6
						* ((double) wordCounter.getSyllableCount() / (double) wordcount)
						- 1.015
						* ((double) wordcount / (double) wordCounter
								.getSentenceCount());
				String result = String.format(
						"%4s %4s\n%4s %d\n%4s %d\n%4s %d\n%4s %4.2f",
						"Filename:", arg, "Number of syllables",
						wordCounter.getSyllableCount(), "Number of words",
						wordcount, "Number of Sentences",
						wordCounter.getSentenceCount(), "Flesch index",
						readability);

				System.out.println(result);
			}
		}
	}

	private static void startGUI() {
		UI ui = new UI();
		ui.setVisible(true);
	}
}
