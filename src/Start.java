public class Start extends State {
	private WordCounter wordcounter;

	public Start(WordCounter wordcounter) {
		this.wordcounter = wordcounter;
		wordcounter.syllable = 0;
	}

	public void handle(char c) {
		if (c == 'e' || c == 'E') {
			wordcounter.setState(wordcounter.efirst);
		} else if (wordcounter.isVowel(c)) {
			wordcounter.setState(wordcounter.vowels);
			;
			wordcounter.syllable++;
		} else if (Character.isLetter(c))
			wordcounter.setState(wordcounter.consonant);
		else
			wordcounter.setState(wordcounter.nonWord);
		System.out.println("Start");
	}

}
