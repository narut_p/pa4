
public class Vowels extends State{
	private WordCounter wordcounter;
	public Vowels(WordCounter wordcounter) {
		this.wordcounter = wordcounter;
	}
	public void handle(char c) {
		if (wordcounter.isVowel(c))
			wordcounter.setState(wordcounter.vowels);
		else if (Character.isLetter(c) || '-' == c)
			wordcounter.setState(wordcounter.consonant);
		else
			wordcounter.setState(wordcounter.nonWord);
	}

}
