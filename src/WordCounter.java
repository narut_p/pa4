import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Scanner;

/**
 * A class that count a word.
 * 
 * @author Narut Poovorakit
 * @version 24.03.2015
 *
 */
public class WordCounter {
	/** An enum of state */
	

	/** a number of syllable */
	private int getSyllable = 0;
	public int syllable;;
	private WordCounter wordcounter;
    private int sentCount = 0;
	public State nonWord = new Non_word(this);
	public State consonant = new Consonant(this);
	public State start = new Start(this);
	public State vowels = new Vowels(this);
	public State efirst = new E_First(this);
	private State state;

	private int sentence;

	/**
	 * A method that count a word
	 * 
	 * @param instream
	 *            is a source that sended.
	 * @return the number of word.
	 */
	public int countWords(InputStream instream) {
		int count = 0;
		Scanner scan = new Scanner(instream);
		while (scan.hasNext()) {
			if (syllable == 0)
				count += 0;
			else
				count++;
			getSyllable += countSyllables(scan.next());

		}
		return count;
	}

	public void setState(State state) {
		this.state = state;
	}

	public int countSentences(String word) {
		sentence = 0;
		state = start;
		String check = "no";
		String Word = word
				.replaceAll("---", " ")
				.replaceAll("--", " ")
				.replaceAll(
						"\\'|\\,|\\|\"|\\@|\\#|\\!|\\^|\\%|\\$|\\&|\\(|\\)|\\_|\\=|\\:",
						"");
		for (char c : Word.toCharArray()) {
            if( c=='\'' || c=='\"'){
            	this.getSyllable++;
            }
			if(c == '.') {
				check = "yes";

			}
			
			if(check.equals("yes")) {
				if(c == ' ') {
					sentence++;
					check = "no";
				}
			}
			state.handle(c);
		}
		
		return sentence;
	}
	

	/**
	 * 
	 * @param url
	 *            is a link that sended.
	 * @return the number of word that be counting.
	 */
	public int countWords(URL url) {
		try {
			return countWords(url.openStream());
		} catch (IOException e) {
			e.printStackTrace();
			return 0;
		}
	}

	// public int countSentences() {
	//
	// }
	public int countSyllables(String word) {
		syllable = 0;
		state = start;

		String Word = word
				.replaceAll("---", " ")
				.replaceAll("--", " ")
				.replaceAll(
						"\\'|\\,|\\|\"|\\@|\\#|\\!|\\^|\\%|\\$|\\&|\\(|\\)|\\_|\\=|\\.|\\:",
						"");
		for (char c : Word.toCharArray()) {
      
			state.handle(c);
		}
		if (state.equals(consonant)) {
			
			if (Word.charAt(Word.length() - 1) == '-'){
				syllable = 0;

			}
		}
		if (syllable == 0 && state == efirst){

			syllable = 1;
		}
		if (state == nonWord){
			this.sentCount++;
			syllable = 0;
		}

		getSyllable = syllable;
		return syllable;
	}

	/**
	 * 
	 * @param c
	 *            is a letter that been sending.
	 * @return true if it was a vowels. false if it was it isn't a vowels.
	 */
	public boolean isVowel(char c) {
		String vowel = "AEIOUaeiou";
		for (char charVowel : vowel.toCharArray()) {
			if (c == charVowel)
				return true;
		}
		return false;
	}

	/**
	 * 
	 * @return a syllable.
	 */
	public int getSyllableCount() {

		return this.getSyllable;
	}

	public int getSentenceCount(){
		return this.sentCount;
	}
	
}