
public class Consonant extends State{
	private WordCounter wordcounter;
	public Consonant(WordCounter wordcounter) {
		this.wordcounter = wordcounter;
		wordcounter.syllable = 0;
	}
public void handle(char c) {

	if (c == 'e' || c == 'E') {
		wordcounter.setState(wordcounter.efirst);
	} else if (wordcounter.isVowel(c) || Character.toUpperCase(c) == 'Y') {
		wordcounter.setState(wordcounter.vowels);
		wordcounter.syllable++;
	} else if (Character.isLetter(c) || c == '-')
		wordcounter.setState(wordcounter.consonant);
	else
		wordcounter.setState(wordcounter.nonWord);
	}

}
